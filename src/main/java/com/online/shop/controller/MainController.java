package com.online.shop.controller;

import com.online.shop.dto.*;
import com.online.shop.service.CustomerOrderService;
import com.online.shop.service.ProductService;
import com.online.shop.service.ShoppingCartService;
import com.online.shop.service.UserService;
import com.online.shop.validator.ChosenProductValidator;
import com.online.shop.validator.ProductValidator;
import com.online.shop.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductValidator productValidator;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private CustomerOrderService customerOrderService;
    @Autowired
    private ChosenProductValidator chosenProductValidator;


    @GetMapping("/addProduct")
    public String addProductPageGet(Model model,
                                    @RequestParam(value = "productAddedSuccessfully", required = false) Boolean productAddedSuccessfully) {
        menuPageGet(model);
        if (userService.userIsAdmin()) {
            ProductDto productDto = new ProductDto();
            model.addAttribute("productDto", productDto);
            if (productAddedSuccessfully != null && productAddedSuccessfully) {
                model.addAttribute("message444", "Product was added successfully in the shop offer!");
            }
            return "addProduct";
        } else {
            return "redirect:/home";
        }
    }

    @PostMapping("/addProduct")
    public String addProductPagePost(@ModelAttribute ProductDto productDto, BindingResult bindingResult,
                                     @RequestParam("productImage") MultipartFile multipartFile, Model model,
                                     RedirectAttributes redirectAttributes) throws IOException {
        productValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()) {
            menuPageGet(model);
            return "addProduct";  // fără acel redirect:/ (de mai jos) întoarce aceeași pagină cu câmpurile
            // completate așa cum le-a băgat utilizatorul
        }
        productService.addProduct(productDto, multipartFile);
        redirectAttributes.addAttribute("productAddedSuccessfully", true);
        return "redirect:/addProduct"; // cu acest redirect:/ întoarce aceeași pagină cu câmpurile goale
    }

    @GetMapping("/home")
    public String homepageGet(Model model) {
        menuPageGet(model);
        List<ProductDto> productDtoList = productService.getAllProductDto();
        model.addAttribute("productDtoList", productDtoList);
        System.out.println(productDtoList);
        return "homepage";
    }

    @GetMapping("/register")
    public String registerPageGet(Model model,
                                  @RequestParam(value = "userAddedSuccessfully", required = false) Boolean userAddedSuccessfully) {
        System.out.println("Am accesat pagina de reghe-register!");
        System.out.println("A fost inregistrat userul introdus? ... " + userAddedSuccessfully);
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        if (userAddedSuccessfully != null && userAddedSuccessfully) {
            model.addAttribute("message888", "User was added successfully!");
        }
        return "register";
    }

    @PostMapping("/register")
    public String registerPagePost(@ModelAttribute UserDto userDto, BindingResult bindingResult,
                                   RedirectAttributes redirectAttributes) {
        userValidator.validate(userDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.addUser(userDto);

        redirectAttributes.addAttribute("userAddedSuccessfully", true);
        return "redirect:/register";
    }

    @GetMapping("/login")
    public String loginPageGet(Model model, @ModelAttribute("userAddedSuccessfully") String userAddedSuccessfully) {
        System.out.println("Am intrat prin pagina de easy-login la pagina căutată (după autentificare)!");
        model.addAttribute("userAddedSuccessfully", userAddedSuccessfully);
        return "login";
    }

    @GetMapping("/loginError")
    public String loginErrorPageGet() {
        return "loginError";
    }

    @GetMapping("/product/{productId}")
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model) {
        menuPageGet(model);
        Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
        if (optionalProductDto.isEmpty()) {
            return "productError";
        }
        ProductDto productDto = optionalProductDto.get();
        model.addAttribute("productDto", productDto);
        System.out.println("Am dat click pe produsul cu ID-ul " + productId);
        ChosenProductDto chosenProductDto = new ChosenProductDto();
        model.addAttribute("chosenProductDto", chosenProductDto);
        return "viewProduct";
    }

    @PostMapping("/product/{productId}")
    public String viewProductPost(@PathVariable(value = "productId") String productId, Model model,
                                  @ModelAttribute("chosenProductDto") ChosenProductDto chosenProductDto,
                                  BindingResult bindingResult) {
        menuPageGet(model);
        if (userService.userIsAdmin()) {
            chosenProductValidator.validateStockAdjustment(chosenProductDto, productId, bindingResult);
            chosenProductValidator.validatePriceAdjustment(chosenProductDto, productId, bindingResult);
            if (bindingResult.hasErrors()) {
                Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
                ProductDto productDto = optionalProductDto.get();
                model.addAttribute("productDto", productDto);
                return "viewProduct";
            }
            productService.adjustStock(chosenProductDto, productId);
            productService.adjustSellingPrice(chosenProductDto, productId);
        } else {
            chosenProductValidator.validate(chosenProductDto, productId, bindingResult);
            if (bindingResult.hasErrors()) {
                Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
                ProductDto productDto = optionalProductDto.get();
                model.addAttribute("productDto", productDto);
                return "viewProduct";
            }
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            shoppingCartService.addToCart(chosenProductDto, productId, loggedInUserEmail);
        }
        return "redirect:/product/" + productId;
    }

    @GetMapping("/cart")
    public String cartPageGet(Model model) {
        menuPageGet(model);
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
        model.addAttribute("shoppingCartDto", shoppingCartDto);
        System.out.println("ShoppingCartDto este " + shoppingCartDto);
        return "cart";
    }

    @PostMapping("/cart/remove/{cartItemId}")
    public String cartRemoveItem(@PathVariable (value = "cartItemId") String cartItemId){
        shoppingCartService.removeShoppingCartItem(cartItemId);
        return "redirect:/cart";
    }

    @PostMapping("/cart")
    public String cartPagePost(Model model) {
        menuPageGet(model);

//
//        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
//        for (ShoppingCartItemDto item : shoppingCartDto.getItems()) {
//            productService.updateShoppingCartItemDto(item);
//        }

//        ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
//        model.addAttribute("shoppingCartDto3366", shoppingCartDto);
//        System.out.println("ShoppingCartDto este " + shoppingCartDto);

        return "cart";
    }

    @GetMapping("/checkout")
    public String checkoutPageGet(Model model) {
        menuPageGet(model);
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
        model.addAttribute("shoppingCartDto", shoppingCartDto);

        UserDetailsDto userDetailsDto = userService.getUserDetailsDtoByEmail(loggedInUserEmail);
        model.addAttribute("userDetailsDto", userDetailsDto);

        return "checkout";
    }

    @PostMapping("/sendOrder")
    public String sendOrderPagePost(@ModelAttribute UserDetailsDto userDetailsDto) {
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        customerOrderService.addCustomerOrder(loggedInUserEmail, userDetailsDto);
        return "confirmation";
    }

    @GetMapping("/ordersTracking")
    public String ordersTrackingPageGet(Model model) {
        menuPageGet(model);
//        List<CustomerOrderDto> customerOrderDtoList = new ArrayList<>();
        List<CustomerOrderDto> customerOrderDtoList = customerOrderService.getAllCustomerOrdersDto();
        model.addAttribute("customerOrderDtoList222", customerOrderDtoList);
        return "ordersTracking";
    }

    @GetMapping("/myOrdersTracking")
    public String myOrdersTrackingPageGet(Model model) {
        menuPageGet(model);
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        List<CustomerOrderDto> customerOrderDtoList = customerOrderService.getCustomerOrdersDtoByUserEmail(loggedInUserEmail);
        model.addAttribute("customerOrderDtoList777", customerOrderDtoList);
        return "myOrdersTracking";
    }

    private void menuPageGet(Model model) {
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        UserDto userDto = userService.getUserDtoByEmail(loggedInUserEmail);
        model.addAttribute("userDto91", userDto);

        ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
        model.addAttribute("shoppingCartDtoYY88", shoppingCartDto);
    }
}
