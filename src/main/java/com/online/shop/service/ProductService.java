package com.online.shop.service;

import com.online.shop.dto.ChosenProductDto;
import com.online.shop.dto.ProductDto;
import com.online.shop.entities.Product;
import com.online.shop.mapper.ProductMapper;
import com.online.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;

    public void addProduct(ProductDto productDto, MultipartFile multipartFile) {
        Product product = productMapper.map(productDto, multipartFile);
        productRepository.save(product);
    }

    public List<ProductDto> getAllProductDto() {
        List<Product> productList = productRepository.findAll();
        List<ProductDto> productDtoList = new ArrayList<>();
        for (Product product : productList) {
            ProductDto productDto = productMapper.map(product);
            productDtoList.add(productDto);
        }
        return productDtoList;
    }

    public Optional<ProductDto> getOptionalProductDtoById(String stringId) {
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(stringId));
        if (optionalProduct.isEmpty()) {
            return Optional.empty();
        }
        Product product = optionalProduct.get();
        ProductDto productDto = productMapper.map(product);
        return Optional.of(productDto);
    }
    public void adjustStock(ChosenProductDto chosenProductDto, String productId) {
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        Product product = optionalProduct.get();
        Integer adjustmentQuantity = Integer.valueOf(chosenProductDto.getQuantity());
        Integer adjustedStock = product.getQuantity() + adjustmentQuantity;
        if(adjustedStock>=0){
            product.setQuantity(adjustedStock);
            productRepository.save(product);
        }
    }
    public void adjustSellingPrice(ChosenProductDto chosenProductDto, String productId) {
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        Product product = optionalProduct.get();
        Double adjustedPrice = Double.valueOf(chosenProductDto.getPrice());
        if (adjustedPrice > 0) {
            product.setPrice(adjustedPrice);
            productRepository.save(product);
        }
    }
}
