package com.online.shop.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class ChosenProductDto {
    private String quantity = "1";
    private String price = "1";
    private ProductDto productDto;
}
