package com.online.shop.validator;


import com.online.shop.dto.UserDto;
import com.online.shop.entities.User;
import com.online.shop.repository.UserRepository;
import com.online.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;
import java.util.regex.Pattern;


@Service
public class UserValidator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private boolean isNotValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        return !pat.matcher(email).matches();
    }

    public void validate(UserDto userDto, BindingResult bindingResult) {

        if (userDto.getFullName() == null || userDto.getFullName().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "fullName",
                    "Write the name");
            bindingResult.addError(fieldError);
        }

        if (userDto.getEmail() == null || userDto.getEmail().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "email",
                    "Write the email address!");
            bindingResult.addError(fieldError);
        } else if (isNotValid(userDto.getEmail())) {
            FieldError fieldError = new FieldError("userDto", "email",
                    "You have to write the email address!");
            bindingResult.addError(fieldError);
        } else {
            Optional<User> optionalUser = userRepository.findByEmail(userDto.getEmail());
            if (optionalUser.isPresent()) {
                FieldError fieldError = new FieldError("userDto", "email",
                        "Email is already in use!");
                bindingResult.addError(fieldError);
            }
        }

        if (userDto.getPassword() == null || userDto.getPassword().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "password",
                    "You must enter a password!");
            bindingResult.addError(fieldError);
        } else if (userDto.getPassword().length() < 8) {
            FieldError fieldError = new FieldError("userDto", "password",
                    "Password must have minimum 8 characters!");
            bindingResult.addError(fieldError);
        } else if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            FieldError fieldError = new FieldError("userDto", "confirmPassword",
                    "Passwords must match!");
            bindingResult.addError(fieldError);
        }

        if (userDto.getAddress() == null || userDto.getAddress().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "address",
                    "You must enter your delivery address!");
            bindingResult.addError(fieldError);
        } else if (userDto.getAddress().length() > 70) {
            FieldError fieldError = new FieldError("userDto", "address",
                    "Address must contain maximum 70 characters!");
            bindingResult.addError(fieldError);
        }

        String stringUtils = "dfdfd";
        stringUtils.isBlank();

    }
}