package com.online.shop.entities;

import com.online.shop.entities.ChosenProduct;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String category;
    private Double price;
    private String description;
    private Integer quantity;

    @Lob
    @Column(columnDefinition = "BLOB")
    private byte[] image;

    @OneToMany(mappedBy = "product")
    private List<ChosenProduct> chosenProducts;

}